/* Завдання під номером 15. Зробити программу, що буде визначати, чи є задане 6-ти значне число "щасливим".
 */

#include <stdio.h>
//Для користування функцією вихода з программи exit()
#include <stdlib.h>
//Оскільки ми будемо коритсуватися смінними типом boolean, то нам треба підключити бібліотеку
#include <stdbool.h>
//Оскільки кількість потрібних цифр у числі не змінна, тому записуємо їх у константу
#define DIGIT_AMOUT 6

//Створюю функцію, яка перевіряє, чи є заданний білет 6-ти значним
void ticket_checker(int ticket_digits_number) {
  if(ticket_digits_number != DIGIT_AMOUT) {
    //Якщо число невірне, то программа завершує свою роботу
    exit(0);
  }
}

//Створюю функцію, що рахуе кількість цифр у заданному числі
int digit_counter(long long n) {
  int count = 0;
  do {
    n /= 10;
    ++count;
  } while (n != 0);
  return count;
}

bool happiness_checker(int first_number, int second_number) {
  if(first_number == second_number) {
    return true;
  } else {
    return false;
  }
}

int main() {
  //Білет
  long long ticket;
  //Змінна, яка включає в себе кількість чисел з поданого білету
  int ticket_digits_number = digit_counter(ticket);

  // Перевіряю, чи є задане число вірним
  ticket_checker(ticket_digits_number);

  //Створюю масив, для чисел білету, щоб порахувати, чи є він щасливий
  int ticket_digits[DIGIT_AMOUT];
  //Створюю змінну, для збереження результату программи
  bool happiness;

  //Записую у массив змінну з заданим числом
  int k = 0;
  while (ticket != 0)
  {
      ticket_digits[k] = ticket % 10;
      ticket /= 10;
      k++;
  }

  //Створюю змінні, які включауть у себе сумму 3-ох перших й 3-ох останніх елементів масива
  int first_numbers = ticket_digits[0] + ticket_digits[1] + ticket_digits[2];
  int second_numbers = ticket_digits[3] + ticket_digits[4] + ticket_digits[5];

  //Якщо ці змінні дорівнюють одне одній, то білет є щасливий
  happiness = happiness_checker(first_numbers, second_numbers);

  return 0;
}

